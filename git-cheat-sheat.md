<<<<<<< HEAD
# GIT INITIALIZE REPOSITORY
```
git init
```

# GIT STATUS 
Check the Status of Your Files (Untracked, Staged or Commited), it also shows the branch you are on
```
git status
```
Short Status with -s or --short option 
New files that aren’t tracked have a ?? next to them, new files that have been added to the staging area have an A, modified files have an M and so on. There are two columns to the output - the left-hand column indicates the status of the staging area and the right-hand column indicates the status of the working tree.
```
git status -s
```

# GIT ADD
To track new files
```
git add [file]
``` 

# GIT DIFF
To know exactly what you changed (not just which files were changed given by git status), but not staged yet, you can use the git diff command
```
git diff
```
Otherwise use to compare your staged changes to your last commit
```
git diff --staged
```
or
```
git diff --cached
```

# GIT LOG
Do summary
```
git log
git log -p or --patch
git log -1 
```
log several format !
```
git log --graph
git log --pretty=oneline
git log --pretty=short
git log --pretty=full
git log --pretty=fuller
git log --stat

```

# GIT MOVING FILES
To track file movement
```
git mv [file_from] [file_to]
```

# GIT COMMIT
To commit the changes. Anything that is still unstaged — any files you have created or modified that you haven’t run git add on since you edited them — won’t go into this commit.
```
git commit
```

# GIT RM
Removing files from the tracked files 
```
git rm [file]
```
To keep the file in your working tree but remove it from your staging area
```
git rm --cached [file]
```
=======
<<<<<<< HEAD
Git Cheat Sheet
======================================================
=======
```
git clone
```
>>>>>>> 35bb6c9894ad142b25737d03c35bb8cfa9c9a85b
>>>>>>> a316d12305b7218194dbe5e32f48914156605c8e

# GIT BRANCHING
Create the branch on your local machine and switch in this branch : 
```
git checkout -b [name_of_your_new_branch]
```
Push the branch on server : 
```
git push origin [name_of_your_new_branch]
```
See all branches : 
```
git branch (* is the branch you are currently in)
```
Add a new remote for your branch : 
```
git remote add [name_of_your_remote] 
```
Push changes from your commit into your branch :
```
git push [name_of_your_new_remote] [name_of_your_branch]
```
Update your branch with the original branch from official repository : 
```
git fetch [name_of_your_remote]
```
Merge changes, if your branch is derivated from develop
```
git merge [name_of_your_remote]/develop
```
Delete a branch on your local filesystem : 
```
git branch -D [name_of_your_new_branch]
```
Delete the branch on server : 
```
git push origin :[name_of_your_new_branch]
```
\newpage

# GIT MERGE / SYNC A FORK

_origin_ remote points to your forked repo; _upstream_ is the repo that you forked from.  
Tell your forked repo which repo is his upstream (mother) :
```
git remote add upstream <repo-location>
```
See which is the upstream/origin repo of your current one:
```
git remote show upstream/origin
```
See which remote points to what:
```
git remote -v
```

Fetch for changes in upstream mother repo :
```
git fetch upstream
```
Check out your fork's local master branch.
```
git checkout master
```
Merge forked repo (_origin_) with its upstream repo
```
git merge upstream/master
```
Resolve merge conflicts manually with editor then git commit and push.

